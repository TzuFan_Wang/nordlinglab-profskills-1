# 2021/09/30 #
- I found the topic for the second lecture very important, because it helps us to recognize fake news from real news.
- I liked each group's focus for the question Is the world getting better or worse, I think they were all different and interesting.
- I really like to work with my teammates, they're all very responsible, and we all contribute to do the assignments. 

# 2021/10/07
- In general, economics is not a topic a enjoy to discuss, because is so complex. But, for this class, the elements shown helped me to understand a little better the way it fluctuates and the impact of that fluctuation in our life.
- I already had Finances classes back in High School, and thanks to that I wasn't completely lost, buy the're so many terms I wasn't familiar with and with this class I became more related to them.
- I wasn't aware of the economical situation here in Taiwan, I found it really interesting.

# 2021/10/14 #
- Today's Ted Talks were my favorites until now. 
- I didn't really understand what fascisim was about until I saw today's video about it. It was simple but pretty accurate. Actually, the thing that man said about the democracy and how it works, made me analyze the parameters I consider when it comes to select a candidate, and that I should start to think more rationally about those kind of topics, specially during the election times.
- The second Ted Talk was the one that moved me the most. I found myslef so overwhelmed by the things that man said, and by the way he saw and now sees people and situations. For me, he just proved that people can change for good.
- I would really like to mention that when he said "Go out there, and find the person that least deserves your compassion and give it to them, because they're who need it the most" it made me cry. It completely change my perspective about people who may make mistakes in life and realize it, but don't have a chance to prove them wrong because nobody thinks they worth it. It reminds me to a phrase from a series, where an inmate asks to the guard why does she treat them so nicely, and not with violence like the other guards. And the guard says that is because the only difference between them is that the inmates were caught for their mistakes in life.

# 2021/10/21 #
- I've been always aware that a good state of mind has a lot to do with the physical state of our body, the instinct to cover our physical necessities (as it is exposed in Maslow's pyramid), but I never understood the process from the physical to the mental state until I saw today's video about it. I think is something I would like to apply in my everyday life.
- I'm used to do regular exercise since my childhood (I'm a ballerina since I'm 10 years old) and it is because of that I never really noticed the good effects on doing exercise until now, that I don't have the same amount of physical exercise per week since I started College. The days were I do exercise from the days I don't definitley hit different and now I can totally perceive this difference as for my mood, my appetite, my focus, sleep quiality etc., suddenly improve the second after I finish exercising. 

# 2021/10/28 #
- In the real life depression is a common topic, specially when it comes to College students. The pressure, the stress, all these factors only increase the feelings that cause these condition. The best thing that a person affected could do is ask for help, of any kind, but specially from a professional that will know how to treat it the best way.
- It is really necessary to talk about this kind of topics, because the more we know about this, the better we will understand it in order to help people. 

# 2021/11/04 #
- Pursue happiness is an imagined reality that has lead people to feel incomplete, that their effort is not enough or that their life is meaningless. To have a purpose, instead, is usually related to this idea, but it's so different when it comes to give a real and tangible meaning to their lives. I've always trusted that my purpose should be something huge, that at least changes a significant amount of people lives to be considered a valid purpose, but after seeing the Ted Talk about this topic, it really made me realize that there's no ruler nor parameter to state a purpose is valid enough or not, it's just about finding something that really fills your soul and your heart when you do it and that contributes, for minimal it is, to improve someone's life.

# 2021/11/11 #
- I took Legislation and Politics class back in High School, so some terms introduced in the video sounded familiar to my understanding.
- I think the documentary was really interesting, and what I liked the most from watching it was the fact it exposed another side of the legal system and the nature of laws that we usually don't think about or that we tend to confuse for the same thing.
- It made me realize how powerful our legal person is and how anyone could basically own us only by having our personal information to link it to our company.
- It is important to know the law of the place we live in, because the more we are aware of what our rights are, the less exposed we find ourselves on situations where higher status members of society (as police officers for example) may take advantage of our ignorance to overpass us.

# 2021/11/25 #
- This was our first physical lecture. I think it was fun, interactive, and watching the videos became really more interesting in the middle of the class. It was nice to be in the company of another people during the class.
- If I'm being honest, I didn't understand some of the topics exposed on the video, but , I agree we deliberately let others to interact with our privacy and behavior just by being part of huge social media masses. It's insane how it works.

# 2021/12/02 #
- Today I learnead about perspective. I don't really know how it started, but since always I had the idea stuck in my mind that to make a real change it was necessary to make a noticeable large scale action. But, what the professor said, about the purspose of this course, made me realize that whatever action we take, small or big, it actually helps to change something, no matter the scale. And that helps us to get to know our own power.
- The church thing made me think a little. Is there any possibility that they find it so annoying because they are not familiar with the masses and the songs they involve? I mention this, because back in my country, I always lived near churches and yes, during the masses they may become noisy, but never found that disturing. On the other hand, when I just arrived to Taiwan, I get to experience one religious celebration in which the people played drums, played with firecrackers, stopped the traffic, and at that time I found that so annoying. Now that I understand what was that about, I think it really wasn't a big deal. So, once again, maybe it's all about perspective. Maaaybe. Maybe the old ladies just get too excited singing hahahaha

# 2021/12/09 #
- While doing the news analysis I discovered some similarities between american countries and european countries when it comes to news reporting and is interesting how the news are told depending on the country. (ideas, content, humor, etc).
- Today I learned what novel entities are:) (we don't have that in Chem Lab hahahaha)

# 2021/12/16 #
- I find the work that must be done for the projects too extensive, but I think the actions are good and we might be able to actually help to improve someone's life by the end of the course ponele.
- I was impressed by the differences in the voting results depending on the voting method used. I also think, that's why sometimes we have the tendency, as a whole, to ignore good ideas or even our common sense just for being part of the majority in a precise moment.
- I just wanted to mention this course and my friend Dave encouraged me to start therapy again, which I did last week:)
