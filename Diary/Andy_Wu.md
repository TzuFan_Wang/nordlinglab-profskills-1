This diary file is written by Andy Wu E24085327 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* I've decided to continue on this course since it helped me improve my researching skill and get to understand what's going on in the world.
* For my group's previous presentation, I've learned not to give credits or mention a member's name if the person didn't contribute and is no longer a part of our group.
* I hope that this class would inspire me to see the opportunity and pathway to make in the future's society.

# 2021-10-7 #

* Money types I learned are officially issued legal tender or fiat-types, representative, and even bit-coin.
* Printing more money during Chinese new year causes price to increase.
* I learned that when we take a loan from the bank, money will be created and not borrowed from other people's account.

# 2021-10-14 #

* I like the idea of professor giving students the opportunity to get presentation points by creating videos instead of presenting directly in class.
* I hope that everyone would get the minimum points for the presentation. When everyone gets their minimum score, students who volunteer to present more will get extra points.
* Because there are less people who want to share their dairy, maybe those who volunteer can get extra presentation points.

# 2021-10-21 #

* I agree with improving health care instead of sick care. Why not take care of yourself to live healthier and longer instead of treatment after the discovery of one's sickness?
* Exercising 30 minutes each day helps improve not only your physical health but also your mental health.
* I will consider in changing my university lifestyle in terms of performing other activities, such as exercising and socializing, instead of studying too much each day.

# 2021-10-28 #

* We had to form a supergroup with 3 groups of 3 people, which there were about 9 people in the group. It was interesting to see how we interact with others by having online discussion because it was way different from having face to face conversation.
* We all face the conflict within ourselves, such as having anxiety and depression. Even though it is natural for us to feel those negative emotions, but I also believe that our genetics plays a role in the way we control our mental health.
* There's a popular point of view on suicide, which is "suicide affects people around you". Some people, like me, agree with the quote, but some feels if the quote is terrible as it means people only care about you when you aren't here anymore, and others are selfish in thinking they are more important than those who have suicidal thoughts.

# 2021-11-4 #

* We had a discussion on how to be successful when we grow up and have a job. It is better to know many people and have connections, because life would be easier when we help each other when facing difficulties.
* When you are new in your job's environment, if you don't like the place you are in, then you should quit 2 weeks later. Why wait longer to see if things improves even though there is a high chance that it would get worse.
* The upcomming group project is going to be challenging, since researches have to be made in the library and we had no experience in this before.

# 2021-11-11 #

* It was easy to cooperate with everyone in my supergroup, and each of us did our own tasks well. We held meetings 2 times at the library, and the experience of having group discussion in the discussion room was great!
* I had high expectations for our group's presentation, in which we did tons of researches and managed to think of 3 creative ways to help people in NCKU with the knowledge from our topic.
* For the upcoming supergroup presentation, I hope I can make improvements in researching faster and efficiently so I won't have to waste too much time being lost on what I have to accomplished.

# 2021-12-03 #

* I was lucky to be in a supergroup where everyone participated, and we also went to the library to have a discussion and get things done.
* Even though doing the presentation is super tiring, I got to understand more of the society we live in.
* It is also interesting to listen to others presenting, in which we learn a lot on other people's opinion and ideas.
* In terms of having our phones taken away, I didn't feel insecure that much because we weren't going anywhere anyway. The only problem was it wasn't convenient for us to take pictures of the assignment the professor showed on screen.

# 2021-12-10 #

* The environmental issue has been bothering me, such as global warming and huge amount of products being wasted.
* I learned about 9 planteary boundaries, and they are biosphere integrity, climate change, novel entities, stratospheric ozone depletion, atmospheric aerosol loading, ocean acidification, biogeochemical flows, freshwater use, and land-system change.
* One of the substances made by human that negatively affected the environment is pesticide, which is toxic to bees. With no bees, there will be no fruit, thus no food for all living species.
* One of the bad news I got is getting the topic that I didn't vote for and didn't want to do, but has to do, is the topic of "Convince church to stop noise that disturbs the students living in the female dorm".

# 2021-12-16 #

* The discussion we had was intense, since the professor asked detailed questions and the presenter must answer them seriously.
* The three charts we had to create was challenging, since we had no experience in creating those graphs before.
* I decided to vote for the goup that presented their idea of creating a social media page on instagram and deliver the concept publicly.
* My topic of "Convincing the church to stop loud noises" wasn't eliminated.
* Hopefully the new members who joined my group will contribute much.

# 2021-12-24 #

* Today, I took an opportunity to ask my electric circuit professor questions on my worn out macbook pro mid 2012 charger with peeled of wire to see how dangerous it is. I've even asked him about if we should charge our electronic devices to the maximum, and at what percentage should we charge our devices. It was cool to talk with the professor on the topic outside of what we learn from class, and I hope to learn more stuffs that are more related to the real world rather than from the book.
